***Settings***
Library     SeleniumLibrary

***Variables***
${url}                  https://training-wheels-protocol.herokuapp.com
${check_thor}           id:thor
${check_ironman}        css:input[value='iron-man']


***Test Cases***
Marcando a opção com o id
    Open Browser                                ${url}                  chrome
    Go To                                       ${url}/checkboxes
    Select Checkbox                             ${check_thor}
    Checkbox Should Be Selected                 ${check_thor}
    Sleep                                       5
    Close Browser

# Marcando a opção com CSS Selector
#     [tags]                                      ironman
#     Open Browser                                ${url}                chrome
#     Go To                                       ${url}/checkboxes
#     Select Checkbox                             ${check_ironman}
#     Checkbox Should Be Selected                 ${check_ironman}
#     Close Browser

# Marcando a opção com XPath
#     [tags]                                      panteranegra
#     Open Browser                                ${url}                chrome
#     Go To                                       ${url}/checkboxes
#     Select Checkbox                             xpath://*[@id='checkboxes']/input[7]
#     Checkbox Should Be Selected                 xpath://*[@id='checkboxes']/input[7]
#     Close Browser
